<?php

/*
|-------------------------------------------------------------------
| Model Factories
|-------------------------------------------------------------------
| Here you may define all of your model factories. Model factories
| give you a convenient way to create models for testing and seeding
| your database. Just tell the factory how a default model should
| look.
*/

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {

    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email_address' => $faker->unique()->email,
        'company_id' => $faker->unique()->randomNumber(),
        'role_id' => $faker->randomNumber(),
        'phone_number' => $faker->phoneNumber,
        'last_login' => $faker->unique()->dateTime,
        'password' => password_hash('password', PASSWORD_BCRYPT),
        'language_id' =>$faker->randomNumber(),
        'api_token' => $faker->randomNumber(),
        'created_by' => $faker->dateTime(),
        'status' => $faker->randomNumber(),
        /*'json_data' => $faker ->(),*/
        'remember_token' => $faker->randomNumber(),
    ];
});
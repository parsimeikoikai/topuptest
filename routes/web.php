<?php
//$router->post('/login', ['middleware' => 'cors', 'uses' =>  'LoginController@postLogin');
$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Books endpoints
$router->get('/populateBooks',['middleware' => 'cors', 'uses' =>  'BooksController@populateBooks']);
$router->get('/listBooks',['middleware' => 'cors', 'uses' =>  'BooksController@listBooks']);

//Characters endpoints
$router->get('/fetchCharacters/{name}/{gender}',['middleware' => 'cors', 'uses' =>  'CharacterController@fetchCharacters']);
$router->get('/listCharacters/{book_id}',['middleware' => 'cors', 'uses' =>  'BooksController@listCharacters']);

//Comments endpoints
$router->get('/listComments/{book_id}',['middleware' => 'cors', 'uses' =>  'CommentsController@listComments']);
$router->post('addComments', ['middleware' => 'cors', 'uses' =>  'CommentsController@addComments']);

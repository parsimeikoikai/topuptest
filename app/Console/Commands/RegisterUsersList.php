<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Mail;
use App\Mail\Gmail;
use Illuminate\Console\Command;

class RegisterUsersList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registered:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $details = [
            'title' => 'Thank you for subscribing to my newsletter',
            'body' => 'You will receive a newsletter every Fourth Friday of the month'

        ];
        Mail::to('koikaiparsimei@gmail.com')->send(new Gmail($details));
        \Log::info("Cron is working.");
    }
}

<?php

namespace App\Console;
use App\Jobs\SendEmailJob;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\RegisterUsersList::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */

    protected function schedule(Schedule $schedule)
    {

         //$schedule->job(new SendEmailJob)->everyMinute();
        //$schedule->command(Commands\RegisterUsersList::class, ['--force'])->everyMinute();
         $schedule->command('registered:users')->everyMinute();
//        $schedule->command(Commands\RegisterUsersList::class, ['--force'])->everyMinute();

    }
    protected $routeMiddleware = [

        'APIToken' => \App\Http\Middleware\APIToken::class,
        'cors' => \App\Http\Middleware\Cors::class,

    ];
}
///var/www/h2hapi
/// //* * * * * php /var/www/h2hapi/artisan schedule:run >> /dev/null 2>&1
/// //* * * * * cd /var/www/h2hapi && php artisan registered:users >> /dev/null 2>&1
/// * * * * * cd /var/www/h2hapi && php artisan schedule:run >> /dev/null 2>&1
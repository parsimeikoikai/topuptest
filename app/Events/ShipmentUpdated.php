<?php

namespace App\Events;

class ShipmentUpdated extends Event
{
    public $shipping_details;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $shipping_details)
    {
        $this->shipping_details = $shipping_details;
    }
}

<?php

namespace App\Events;

class UserCreated extends Event
{
    public $user_details;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $user_details)
    {
        $this->user_details = $user_details;
    }
}

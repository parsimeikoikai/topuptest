<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BooksModel extends Model
{
    protected $table = "tbl_books";
    protected $fillable = [
        'name',
        'first_name',
        'character_urls',
        'authors',
        'released_date'];

}


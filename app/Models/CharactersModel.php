<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CharactersModel extends Model
{
    protected $table = "tbl_characters";
    protected $fillable = ['name','gender','born','died'];

}

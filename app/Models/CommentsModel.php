<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentsModel extends Model
{
    protected $table = "tbl_comments";
    protected $fillable = ['comment','book_id','ip_address','utc_time'];

}

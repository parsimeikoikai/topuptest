<?php

namespace App\Http\Controllers;
use App\Models\BooksModel;
use Laravel\Lumen\Routing\Controller;
use App\Models\CharactersModel;
use Illuminate\Http\Request;

class CharacterController extends Controller
{
    //Fetch characters from external API
    /**
     * @OA\Get(
     *     path="/fetchCharacters",
     *     operationId="/fetchCharacters",
     *     tags={"Charachters Data"},
     *     @OA\Parameter(
     *         name="Charachters Data",
     *         in="query",
     *         description="Save characters fetched from external API endpoint to local DB",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Save characters fetched from external API endpoint to local DB",
     *         @OA\JsonContent(
     *             @OA\Property(type="object", ref="#")
     *         ),
     *     ),
     * )
     */
    public function fetchCharacters()
    {

        $url = 'https://anapioficeandfire.com/api/characters';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        //convert json to array
        $characters_data = json_decode($response, TRUE);
        foreach ($characters_data as $data) {

            CharactersModel::create([
                'name' => $data['name'],
                'gender' => $data['gender'],
                'born' => $data['born'],
                'died' => $data['died'],
            ]);
        }
        $msg = count($characters_data).''." Characters have successfully been imported";
        $response = array("msg" => $msg);
        return response()->json($response, 200);

    }
    //filter and list characters
    /**
     * @OA\Get(
     *     path="/filterCharacters/name/gender/age",
     *     operationId="/filterCharacters/name/gender/age",
     *     tags={"Charachters Data"},
     *     @OA\Parameter(
     *         name="Charachters Data",
     *         in="query",
     *         description="Filter and List characters fetched from external API endpoint",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Filter and List characters fetched from external API endpoint",
     *         @OA\JsonContent(
     *             @OA\Property(type="object", ref="#")
     *         ),
     *     ),
     * )
     */
    public function filterCharacters(Request $request)
    {
        $name = $request->name;
        $gender = $request->gender;
        $age = $request->age;
        $condition = [
            'gender' => $gender,
        ];

        $character_list = CharactersModel::where($condition)
            ->orWhere([['name', 'ILIKE', "%" . $name . "%"]])
            ->get();

        foreach ($character_list as $list)
        {

            $row["name"] = $list->name;
            $row["gender"] = $list->gender;
            $data[] = $row;
        }
        return $data;

    }

    /**
     * @OA\Get(
     *     path="/listCharacters/book_id",
     *     operationId="/listCharacters/book_id",
     *     tags={"Charachters Data"},
     *     @OA\Parameter(
     *         name="Charachters Data",
     *         in="query",
     *         description="List charachters on any Book",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *          description="List charachters on any Book",
     *         @OA\JsonContent(
     *             @OA\Property(type="object", ref="#")
     *         ),
     *     ),
     * )
     */
    //list characters in a book
    public function listCharacters(Request $request)
    {
        $condition =
            [
                'id' => $request->book_id
            ];
        $comments_list = BooksModel::where($condition)
            ->get();
        $no = 0;
        foreach ($comments_list as $list)
        {
            $characters_arr = json_decode($list->character_urls);
            $no++;
            $row["id"] = $no;
            $row["character_urls"] = $characters_arr;
            $data[] = $row;
        }
        return $data;
    }
}

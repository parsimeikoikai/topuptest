<?php

namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\CommentsModel;

class CommentsController extends Controller
{
    //add comments
    /**
     * @OA\Get(
     *     path="/addComments",
     *     operationId="/addComments",
     *     tags={"Comments Data"},
     *     @OA\Parameter(
     *         name="Comments Data",
     *         in="query",
     *         description="Add comments from users on each book",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Add comments from users on each book",
     *         @OA\JsonContent(
     *             @OA\Property(type="object", ref="#")
     *         ),
     *     ),
     * )
     */
    public function addComments(Request $request)
    {

        $current_utc = gmdate("Y-m-d H:i:s");;
        $data = [
            'comment' => $request->input('comment'),
            'book_id' => $request->input('book_id'),
            'ip_address' => $request->ip(),
            'utc_time' => $current_utc,
        ];
        if (str_word_count($request->input('comment')) > 500)
        {
            $msg = 'Error!Comment should be less than 500 words';
        }else
        {
            $query =  CommentsModel::create($data);

            if ($query)
            {

                $msg = 'Comment added successfully';
            }else
            {
                $msg = 'Error!Please try again later';
            }
        }

        $response = array("msg" => $msg);
        return response()->json($response);
    }
    //list comments on book
    /**
     * @OA\Get(
     *     path="/listComments/{book_id}",
     *     operationId="/listComments/{book_id}",
     *     tags={"Comments Data"},
     *     @OA\Parameter(
     *         name="Comments Data",
     *         in="query",
     *         description="List comments from users on each book",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List comments from users on each book",
     *         @OA\JsonContent(
     *             @OA\Property(type="object", ref="#")
     *         ),
     *     ),
     * )
     */
    public function listComments(Request $request)
    {
        $condition =
            [
                'book_id' => $request->book_id
            ];
          $comments_list = CommentsModel::where($condition)
              ->orderBy('tbl_comments.id', 'desc')
            ->get();
        foreach ($comments_list as $list)
        {
            $row["comment"] = $list->comment;
            $row["ip_address"] = $list->ip_address;
            $row["utc_time"] = $list->utc_time;
            $data[] = $row;
        }
        if (count($comments_list) < 1)
        {
            $data = array();
        }
        return $data;

    }
    //Count comments on any book
    public function countComments($book_id)
    {
        $condition =
            [
                'book_id' => $book_id
            ];
        $comments = CommentsModel::where($condition) ->get();
        return count($comments);
    }


}

<?php
namespace App\Http\Controllers;

use OpenApi\Annotations\Contact;
use OpenApi\Annotations\Info;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;
use OpenApi\Annotations\Server;
/**
 * @OA\Info(title="API Endpoints", version="0.1")
 */

/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="http://127.0.0.1:8000",
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="API Endpoints",
 *         description="Api description...",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="koikaiparsimei@gmail.com"
 *         ),
 *         @SWG\License(
 *             name="Private License",
 *             url="URL to the license"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Please reach out if you need any help",
 *         url="http..."
 *     )
 * )
 */
class SwaggerController
{

}

<?php

namespace App\Http\Controllers;
use App\Models\CommentsModel;
use App\Models\CharactersModel;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\BooksModel;

class BooksController extends Controller
{
    public function fetchBooks()
    {

        $url = 'https://anapioficeandfire.com/api/books';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        //convert json to array
        $data = json_decode($response, TRUE);
        return $data;

    }
    /**
     * @OA\Get(
     *     path="/listBooks",
     *     operationId="/listBooks",
     *     tags={"Books Data"},
     *     @OA\Parameter(
     *         name="Books Data",
     *         in="query",
     *         description="List Books fetched from external API endpoint",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List Books fetched from external API endpoint",
     *         @OA\JsonContent(
     *             @OA\Property(type="object", ref="#")
     *         ),
     *     ),
     * )
     */
    public function listBooks()
    {
        $books_data = $this->fetchBooks();
        //save data to db
        $names_array = array();
        $no = 0 ;
        foreach ($books_data as $data) {
            $no ++;
            $data = [
                'name'=>$data['name'],
                'authors'=>$data['authors'],
                'released_date'=>$data['released'],
                'comments' => $this->countComments($no)
            ];
            array_push($names_array,$data);
        }

        return $names_array;
    }
    //Count comments on any book
    public function countComments($book_id)
    {
        $condition =
            [
                'book_id' => $book_id
            ];
        $comments = CommentsModel::where($condition) ->get();
        return count($comments);
    }
    //Import books from external API
    /**
     * @OA\Get(
     *     path="/populateBooks",
     *     operationId="/populateBooks",
     *     tags={"Books Data"},
     *     @OA\Parameter(
     *         name="Books Data",
     *         in="query",
     *         description="Save Books data from external API to local DB",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List Books fetched from external API endpoint",
     *         @OA\JsonContent(
     *             @OA\Property(type="object", ref="#")
     *         ),
     *     ),
     * )
     */
    public function populateBooks()
    {
        $books_data = $this->fetchBooks();
        //save data to db
        foreach ($books_data as $data) {
            $characters_ids =  $this->processCharacters($data['characters']);
            BooksModel::create([
                'name' => $data['name'],
                'character_urls' => json_encode($characters_ids),
                'authors' => json_encode($data['authors']),
                'released_date' => $data['released'],
            ]);
        }
        //return response to user
        $msg = count($books_data).''." Books have successfully been imported";
        $response = array("msg" => $msg);
        return response()->json($response, 200);
    }
    //process characters data
    public function processCharacters($characters_array)
    {
        $array = array();
        foreach ($characters_array as $data) {
            $cnr = substr($data, -6);
            $processed_array = substr($cnr, strpos($cnr, "/") + 1);
            array_push($array,$processed_array);
        }
        return $array;

    }




}
